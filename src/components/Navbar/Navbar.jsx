import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link } from 'react-router-dom';
import Cartwidget from "../CartWidget/Cartwidget";

const Menu = () => {
  return (
    <Navbar collapseOnSelect expand="lg" bg="light" variant="dark">
      <Container>
        <Link to='/'>Super Compras</Link>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <Link to='/categoria/controles'>Controles</Link>
            <Link to='/categoria/consolas'>Consolas</Link>
          </Nav>
          <Nav>
            <Nav.Link href="#deets">Carrito</Nav.Link>
            <Link to="/cart">
              <Cartwidget />
            </Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

export default Menu;