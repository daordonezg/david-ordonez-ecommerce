const ItemDetail = ({ product }) => {
    return (
            <div className="row">
                <div className="col">
                    <img src={product.foto} className="w-25" />
                    <p>Categoría: {product.categoria}</p>
                    <p>Precio: {product.price}</p>
                    <p>stock: {product.stock}</p>
                </div>
            </div>

    )
}

export default ItemDetail
