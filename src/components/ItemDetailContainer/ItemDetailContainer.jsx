import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import ItemDetail from "../ItemDetail/ItemDetail"
import { gFetch } from "../../utils/gFetch"

const ItemDetailContainer = () => {
    const [product, setProduct] = useState({})
    const [loading, setLoading] = useState(true)

    const {productId} = useParams()

    console.log(productId)

    useEffect(()=>{
      gFetch(productId)
      .then(resp => setProduct(resp))
        .catch(err => console.log(err))
        .finally(() => setLoading(false))
    },[])

    console.log(product)
  return (
    <div >
        {loading ? 
                <h2>Cargando...</h2> 
            : 
                <ItemDetail product={product} />
        }
        
    </div>
  )
}

export default ItemDetailContainer