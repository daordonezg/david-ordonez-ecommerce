import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { gFetch } from '../../utils/gFetch'
import ItemList from '../ItemList/ItemList';

const Itemlistcontainer = ({ greeting }) => {
    const [products, setProducts] = useState([]);
    const [loading, setLoading] = useState(true);
    const { categoriaId } = useParams();
    console.log(categoriaId);
    useEffect(() => {
        if (categoriaId) {
            gFetch()
                .then(resp => setProducts(resp.filter(prod => prod.categoria === categoriaId)))
                .catch(err => console.log(err))
                .finally(() => setLoading(false))

        } else {
            gFetch()
                .then(resp => setProducts(resp))
                .catch(err => console.log(err))
                .finally(() => setLoading(false))
        }


    }, [categoriaId]);

    const cambiarEstado = () => {
        setBool(!bool)
    }

    return (
        loading ?
            <h2>Cargando...</h2> :
            <div>
                <h1>Catalogo</h1>
                <ItemList products={products} />
            </div>
    );
}

export default Itemlistcontainer;