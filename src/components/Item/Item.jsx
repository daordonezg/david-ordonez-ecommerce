import { Link } from "react-router-dom"
import './Item.css'

const Item = ({ product }) => {
  return (
    <div className="boxProductos d-inline-flex p-2 w-25">
        <Link to={`/item/${product.id}`}>
            <div className="capProduct w-25">
                {product.name}
            </div>
            <div className='card-body w-25'>
                <center>
                    <img src={product.foto} className="w-25" />
                </center>
            </div>
            <div className='card-footer w-25'>
                categoria: {product.categoria}
            </div>
        </Link>
    </div> 
  )
}

export default Item