import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom'



import Menu from './components/Navbar/Navbar'
import Itemlistcontainer from './components/Itemlistcontainer/Itemlistcontainer'

import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import ItemDetailContainer from './components/ItemDetailContainer/ItemDetailContainer';

function App() {

  return (
    <div className="App">
      <BrowserRouter>
        <Menu titulo="Super tienda"/>
        <Routes>
          <Route path = '/' element= {<Itemlistcontainer greeting="Saludos!!"/>}/>
          <Route path = '/categoria/:categoriaId' element= {<Itemlistcontainer greeting="Saludos!!"/>}/>
          <Route path = '/item/:productId' element= {<ItemDetailContainer/>}/>
        </Routes>        
      </BrowserRouter>
    </div>
  )
}

export default App
