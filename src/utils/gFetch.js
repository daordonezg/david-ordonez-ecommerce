// simulación -> mock

// fetch -> promise
let obj = [
    {id: '1', name: 'Control Xbox', categoria: 'controles',stock: '100',price: '1500',  foto:'https://cdn-icons-png.flaticon.com/512/330/330942.png'},
    {id: '2', name: 'Control Switch', categoria: 'controles',stock: '100',price: '1500',  foto:'https://cdn-icons-png.flaticon.com/512/329/329776.png'},
    {id: '3', name: 'Control PS', categoria: 'controles',stock: '100',price: '1500',  foto:'https://cdn-icons-png.flaticon.com/512/2592/2592341.png'},
    {id: '4', name: 'Xbox', categoria: 'consolas',stock: '100',price: '15000',  foto:'https://cdn-icons-png.flaticon.com/512/771/771210.png'},
    {id: '5', name: 'Play Station', categoria: 'consolas',stock: '100',price: '13000',  foto:'https://as2.ftcdn.net/v2/jpg/04/50/60/47/1000_F_450604754_9eYvTdLzQq5PGh6VyytOO5uWGhiQ8wQt.jpg'}
]

export const gFetch = (id) => {
    // comporobaciones

    return new Promise( ( resuleto, rechazada ) => {
        // ACCIONES
        
        setTimeout(()=>{
            resuleto( id ? obj.find( item => item.id === id ) : obj )
        }, 2000)
        // rechazada('promesa rechazada')
    })
    // .catch()
}

    
